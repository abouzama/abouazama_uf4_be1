# JAVA_UF4

### Bloc Exercici abouazama JAVA 

>[!IMPORTANT]  
> ya lo fet cada exercici al seu classe no mes per separar els classes dels Exrcicis i tots els exercicis tenen un Class Run.java.


***Exercici 1***   
en el Primer Exercici qui lo fet es utilitzar els metodes length() i el metode charAt()  
i el length() es per saber la longitude del Text intrduit per el Usuari   
i el charAt() lo qui fa es agafa sol un caracter del la posicio qui vols  
i la iteracio lo qui fa es anan combroban tots els Caracters del Text un a un 

```java
package abouzama_UF4_Be1_E1;

public class Exercici1 {
	private String Text;
	
	public int comptarVegadesquiapareixlae(){
		int i = 0;
		int comptador=0;
		while(i<Text.length()) {
			if(Text.charAt(i)=='e' || Text.charAt(i)=='E') {
				comptador++;
			}
			i++;
		}
		return comptador;
	}


	public String getText() {
		return Text;
	}


	public void setText(String text) {
		this.Text = text;
	}
	

}

```
#### Run de Exercici 1   
En el la class Run.java la primera etapa es instanciar el Scanner en el meu Class i despres instanciar el Exercici per jo poc accedir als Comportaments del class     
     
	 Exercici 1
```java
package abouzama_UF4_Be1_E1;

import java.util.Scanner;

public class RunExerc {

    public static void main(String[] args) {
        Scanner lectura = new Scanner(System.in);
		    Exercici1 ex1 = null;
            // EXercici 1
		// instanciem un objecte amb un constructor vuid
		    ex1 = new Exercici1();
		    System.out.println("Introdeuix un Text que vols comptar els vegades que apareix la e");
		    ex1.setText(lectura.nextLine());
		    System.out.println("els veagdes que apareix la 'e' al teu Text es " + ex1.comptarVegadesquiapareixlae());
            lectura.close();//Quan acabes amb el Scanner millor tanque
        }
}

            
```
### una execusio del Exercici 1   

![image](images/EX1.png) 

***Exercici 2***   
>   Exercici 2.
Endevinar per teclat una paraula de pas. Si s’endevina cal mostrar el missatge
“Felicitats!!! l'has endevinat”. En cas contrari el missatge a mostrar serà
“Llàstima!!! no l'has endevinat”. Es permetrà 3 intents com a màxim

en aquest Exercici utilitzat elmetode equals per comparar la paraula qui tinc amb la paraula introduida per el usuauri

```java
package abouzama_UF4_Be1_E1;

import java.util.Scanner;

public class Exercici2 {
	private String paraulaaendvina="Hola";
	private String paraula;
	
	
	public boolean comparalaparaulaintroduitambelquitnc() {
		boolean endivinat=false;
		if(paraula.equals(paraulaaendvina)) {
			endivinat=true;
		}
		return endivinat;
	}
	public void jocendvina() {
		Scanner lectura = new Scanner(System.in);
		int i=0;
		//per funciona el programa perfectament hi ha de fer el DO while el DO lo quefa es demana al usuari la paraula per endivinar el qui tinc 
		//i el condicional lo compara la paraula introduit amb el que tinc 
		//en cas de l'usuari l'endvina perfecament surt de la iteracion
		// i el index anar suman els vegades qui intenta el usuari
		do {
			System.out.println("endvina la pararula que tinc");
			 setparaula(lectura.next());
			if (comparalaparaulaintroduitambelquitnc()) {
				System.out.println("Felicitats!!! l'has endevinat");
			} else {
				System.out.println("Llàstima!!! no l'has endevinat");
			}
			i++;
		}while (!comparalaparaulaintroduitambelquitnc() && i<=3);//aquesta iteracio per controla els vegades que els usuari esta endvina 
		if(i>3)System.out.println("Important has passat el maxim vegades qui tens....");
	}
	
	
	//lo fet el set solo por que necssito sol el set i no me interessa el get 
	//set paraula 
	public void setparaula(String paraula) {
		this.paraula=paraula;
	}
}
```
#### Run de Exercici 2   
En el la class Run.java la primera etapa es instanciar el Scanner en el meu Class i despres instanciar el Exercici per jo poc accedir als Comportaments del class Exercici 2
```java
package abouzama_UF4_Be1_E1;

import java.util.Scanner;

public class RunExerc {

    public static void main(String[] args) {
            Scanner lectura = new Scanner(System.in);
            Exercici2 ex2 = null;
            ex2 = new Exercici2();
            System.out.println("----important tens 3 intents----");
            ex2.jocendvina();
            lectura.close();
        }
}
		
```
### una execusio del Exercici 2   
**en cas no l'endvines**    

![image](images/EX2.png)      

**en cas l'endvines**
![image](images/EX2-1.png)
***Exercici 2***  
>Exercici 3.
Introduir un text per teclat i escriure a pantalla el nombre de caràcters.   

en aquest Exercici utilitzat el metode length per saber quants caracters te el Text intrduit
```java
package abouzama_UF4_Be1_E1;

public class Exercici3 {
	private String Text;
	
	
	public void comptarcaractersdelText() {
		System.out.println("el teu Text te "+Text.length()+" Caracters.");
	}

	public String getText() {
		return Text;
	}

	public void setText(String text) {
		Text = text;
	}
	
	
}

```
#### Run de Exercici 3   
En el la class Run.java la primera etapa es instanciar el Scanner en el meu Class i despres instanciar el Exercici per jo poc accedir als Comportaments del class i en aquest utilitzat el nextLin() per que es un Text qui pot te mes de una paraula 
    
	 Exercici 3
```java
package abouzama_UF4_Be1_E1;

import java.util.Scanner;

public class RunExerc {

	public static void main(String[] args) {
		Scanner lectura = new Scanner(System.in);
		Exercici3 ex3 = null;
		ex3 = new Exercici3();
		System.out.println("introdeuix un Text: ");
		ex3.setText(lectura.nextLine());
		ex3.comptarcaractersdelText();
		lectura.close();
	}
}

```

### una execusio del Exercici 4   

![image](images/Ex3.png)     

***Exercici 4***   
>   Exercici 4.
Introduir un text per teclat i escriure a pantalla el nombre de vocals.   

```java
package abouzama_UF4_Be1_E1;
public class Exercici4 {
	private String Text;
	public int comptarVocalsdelText() {
		int it=0,cv=0;
		// Aquesta iteracio lo que fa controla por el index no passa la longitude de Text
		while(it<getText().length()) {
			// el charat() es comprova caacter a caracter si es Vocal o no si es lo comptar con el contador 
			if(Text.charAt(it)=='a' || Text.charAt(it)=='o'  || Text.charAt(it)=='i'  || Text.charAt(it)=='u' || Text.charAt(it)=='e') {
				cv++;
			}
			it++;
		}
		return cv;// retorna quants Vocals hi ha en el Text
	}
	public void setText(String Text) {
		this.Text=Text;
	}
	public String getText() {
		return Text;
	}
}
```
   
      
	  la Class Run
   
   ```java 
package abouzama_UF4_Be1_E1;

import java.util.Scanner;

public class RunExerc {

	public static void main(String[] args){
		Scanner lectura = new Scanner(System.in);
		Exercici4 ex4 = null;
		//Exercici 4
		ex4=new Exercici4();
		System.out.println("introdeuix un Text:");
		ex4.setText(lectura.nextLine());
        System.out.println("el teu Text te "+ex4.comptarVocalsdelText()+" Vocals.");
		lectura.close();
		}
}
   ```   

### una execusio del Exercici 4   

![image](images/EX4.png)    



***Exercici 5***   
>   Exercici 5.
Introduir un text per teclat i escriure a pantalla el nombre de consonants.
(Suposarem que el text introduït està format només per caràcters alfabètics i blancs)   

```java
package abouzama_UF4_Be1_E1;

public class Exercici5 {
	private String Text;
	public int comptarConconantsdelText() {
		int it=0,cc=0;
		while(it<getText().length()) {
			// el charat es comprova caacter a caracter si no es Vocal i no  es saltar Blancs lo compta el comptador
			if(Text.charAt(it)!='a' && Text.charAt(it)!='o' 
					&& Text.charAt(it)!='i'  && Text.charAt(it)!=' '
					&& Text.charAt(it)!='u' && Text.charAt(it)!='e') {
				cc++;//retorna quants caractrs hi ha en el Text introduit
			}
			it++;
		}
		return cc;
	}
	public void setText(String Text) {
		this.Text=Text;
	}
	public String getText() {
		return Text;
	}

}
```
   
      
	  la Class Run
   
   ```java 
package abouzama_UF4_Be1_E1;

import java.util.Scanner;

public class RunExerc {

	public static void main(String[] args){
		Scanner lectura = new Scanner(System.in);
		Exercici5 ex5 = null;
		//Exercici 5
		ex5 = new Exercici5();
		System.out.println("introdeuix un tex que vols comptar els consonats: ");
		ex5.setText(lectura.nextLine());
		System.out.println("el teu Text te "+ex5.comptarConconantsdelText()+" Consonants");
		lectura.close();
		}
}
```   

### una execusio del Exercici 5   

![image](images/EX6.png)     


***Exercici 5***   
> 
Exercici 6.
Introduir un text per teclat i escriure a pantalla el nombre de vegades que apareix la
paraula “home”.
(Suposarem que el text introduït està format només per caràcters alfabètics i blancs)blancs)   

```java
package abouzama_UF4_Be1_E1;

public class Exercici6 {
	private String Text;
	private String parhome="home";
	public int existeixparaulahome() {
		int count=0,i=0;
		String[] paraulesText = Text.split(" ");
		while(i<paraulesText.length){
			 if (paraulesText[i].equalsIgnoreCase(parhome)) {
	                count++;
	            }
			 i++;
		}
	return count;
	}
	public void setText(String text) {
		this.Text=text;
	}
	public String getText() {
		return Text;
	}	
}
```   
   
      
	  la Class Run
   
   ```java 
package abouzama_UF4_Be1_E1;

import java.util.Scanner;

public class RunExerc {
	public static void main(String[] args){
		Scanner lectura = new Scanner(System.in);
		Exercici6 ex6 = null;
		//Exercici 6
		ex6 = new Exercici6();
		System.out.println("introdeuix un text");
		ex6.setText(lectura.nextLine());
		System.out.println("la paraula 'home' existeix :"+ex6.existeixparaulahome()+" vegades");
		lectura.close();
		}
}
``` 
### una execusio del Exercici 6   

![image](images/EX5.png)     
 

 ### Exercici 7
***indexOf() (amb totes les variants):***

    El mètode indexOf(char,int) retorna la primera ocurrència d'una subcadena dins de la cadena donada . Si la subcadena no és troba, retorna -1
per exemple en cas qui lo trobi   
![image](images/ex7.png)   
   
   en cas que no lo trobi     

![image](images/ex7-0.png)    

    

    El mètode indexOf(char,int) retorna la primera ocurrència d'una subcadena dins de la cadena donada després d'aquest index qui dono. Si la subcadena no és troba, retorna -1    per exemple
![image](images/ex7-1.png)    

   
    El mètode indexOf(char) retorna la primera ocurrència d'una subcadena  de la cadena donada . Si la subcadena no és troba, retorna -1   per exemple
![image](images/ex7-2.png)    

    
	El mètode indexOf(char,int) retorna la primera ocurrència d'una subcadena  de la cadena donada després d'aquest index qui dono. Si la subcadena no és troba, retorna -1   per exemple


 ![image](images/ex7-3.png)    

 ***metode isEmpty():***
    
	 el isEmpty()   
	  lo que fa es comprobar un String si esta vuid     
	  no,en cas que la paraula no     
	  esta vuida retorna false i en el contrari retorna true per exemple:

![image](images/ex7-4.png)    
  
 ***metode endswith():***  
    
	el endswiths() lo que fa es comprova si
	aquest String     
	amb el caracter o cadena que yo dono en     
	cas que si    
	 acabe amb aquest caracter o cadena     
	retorna true en el contrari retorna false per exemple
   ![image](images/Ex7-5.png)

 ***metode compareToIgnoreCase():***  
    
	el compareToIgnoreCase() lo que fa es comprova si dos strings (un mayus.. i un altre minus..)    
	si son iguals encas de son iguals retorna 0
	i en cas de no son iguals 
	- si la cadena que esta dins del metode es majúscula i no son iguals retorna un nmero mes gran que 0 
	- i en cas que la cadena que esta dins del metode es una cadena minuscula(petita) retorna un numero negatiu
	per exemple
   ![image](images/EX7-6.png)

   
***metode lastIndexOf():***  
       
	el metode lastindexof()    
	 lo que fa es agafa el index de la ultima aparacio     
	del substring en el Text o la paraula   
	 i en cas de posar una paraula en el indexof(par) retorna el index del ultim substring d'aquesta paraula 
	
	per exemple
   ![image](images/EX7-7.png)
***metode matchs():*** 
      
	  El mètode matchs()    
	  comprova si tota la cadena coincideix amb l'expressió   
	   regular especificada.
      Retorna true si la cadena coincideix amb l'expressió regular,    
	  i false en cas contrari.     

![image](images/EX7-8.png)    
***metode replace():*** 
    
	El mètode replace() substitueix totes les ocurrències d'un caràcter o seqüència de caràcters especificats per un altre caràcter o seqüència de caràcters dins d'una cadena. No utilitza expressions regulars. per exemple
![image](images/EX7-9.png)
***metode replaceAll():*** 
	El mètode replaceAll() substitueix totes les ocurrències    
	d'una subcadena especificada que coincideix amb una    
	expressió regular determinada per una nova      
	especificada. Utilitza expressions regulars per a   
	la concordança de patrons.per exemple
![image](images/EX7-10.png)    
***metode replaceFirst():*** 
	El mètode replaceFirst() substitueix la primera ocurrència d'una    
	subcadena que coincideix amb una expressió regular     
	determinada per una nova cadena especificada. També    
	utilitza expressions    
	regulars per a la concordança de patrons.
![images](images/EX7-11.png)
***metode split():***    

    El mètode split() a Java s'utilitza per dividir una cadena en una array    
    de subcadenes basades en un delimitador  (per exemple un text normal espai) especificat. per exemple (exercici 6) jo utilitzat el metode split per separar el text    
      
***metode substring():***    

    El mètode substring() té dues variants:
    La primera variant pren un únic argument primer Índex i retorna una nova cadena que és una subcadena de la cadena original, començant des de l'índex d'inici fins al final de la cadena.
    La segona variant pren dos arguments primer Index i final Index, i retorna una nova cadena que és una subcadena de la cadena original, començant per primer Index i acabant i abans de Index final.    
![img](images/EX7-13.png)

***metode toCharArray():***    
    

	El mètode toCharArray() de Java s'utilitza per convertir una cadena en una matriu de caràcters  
***metode toLowerCase():*** 
    
	El mètode toLowerCase() de Java s'utilitza per convertir tots els caràcters d'una cadena a minúscules 
	per exemple 

![image](images/EX7-14.png)     

***metode toString():*** 
    
	El mètode toString() a Java s'utilitza per obtenir una representació de cadena d'un objecte. Sovint s'utilitza per convertir un objecte en una cadena. Tanmateix, és important tenir en compte que el comportament de toString() pot variar segons la classe específica i com s'ha implementat.
***metode toUpperCase():***    

    El mètode toUpperCase() s'utilitza per convertir una cadena en lletres majúscules. Retorna una    
	cadena nova que representa    
	la cadena de trucada convertida a majúscules.    
	Aquest mètode no afecta el valor d la cadena per exemple:
![images](images/EX7-15.png)
***metode startsWith():***    
    
	el mètode startsWith() s'utilitza per comprovar si una cadena comença amb un caràcter específic. Retorna true si la cadena comença amb un caracter especificat, i false en cas contrari.   
	per exemple:
![image](images/EX7-17.png)

***metode trim():***  
    
	El mètode trim() a    
	 Java s'utilitza per eliminar qualsevol espai en blanc inicial i final     
	d'una cadena. Retorna una    
	cadena nova després    
	 d'eliminar els espais en blanc inicials i posteriors.
per exemple:
![img](images/EX7-18.png)
***metode valueOf():***

    El mètode valueOf() a Java s'utilitza per convertir diferents tipus de valors en representació de cadena. Està disponible per a diversos tipus de dades i objectes, i el seu ús varia segons el context.
![img](images/EX7-18f.png)




