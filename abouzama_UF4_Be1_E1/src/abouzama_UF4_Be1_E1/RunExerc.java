package abouzama_UF4_Be1_E1;

import java.util.Scanner;

public class RunExerc {

	public static void main(String[] args) {
		Scanner lectura = new Scanner(System.in);
		Exercici1 ex1 = null;
		Exercici2 ex2 = null;
		Exercici3 ex3 = null;
		Exercici4 ex4 = null;
		Exercici5 ex5 = null;
		Exercici6 ex6 = null;
		// EXercici 1
		// instanciem un objecte amb un constructor vuid
		/*ex1 = new Exercici1();
		System.out.println("Introdeuix un Text que vols comptar els vegades que apareix la e");
		ex1.setText(lectura.nextLine());
		System.out.println("els veagdes que apareix la 'e' al teu Text es " + ex1.comptarVegadesquiapareixlae());
		
		// Exercici 2
		ex2 = new Exercici2();
		System.out.println("----important tens 3 intents----");
		ex2.jocendvina();
		
		//EXercici 3
		ex3 = new Exercici3();
		System.out.println("introdeuix un Text: ");
		ex3.setText(lectura.nextLine());
		ex3.comptarcaractersdelText();
		
		//Exercici 4
		ex4=new Exercici4();
		System.out.println("introdeuix un Text:");
		ex4.setText(lectura.nextLine());
        System.out.println("el teu Text te "+ex4.comptarVocalsdelText()+" Vocals.");
        
		//Exercici 5
		ex5 = new Exercici5();
		System.out.println("introdeuix un tex que vols comptar els consonats: ");
		ex5.setText(lectura.nextLine());
		System.out.println("el teu Text te "+ex5.comptarConconantsdelText()+" Consonants");
		
		ex6 = new Exercici6();
		
		System.out.println("introdeuix un text");
		ex6.setText(lectura.nextLine());
		System.out.println("la paraula 'home' existeix :"+ex6.existeixparaulahome()+" vegades");
		*/
		//Exercici 7
		String Text0="Hola que tal que";
		//indexof() amb tots els variantes 
		/*System.out.println(Text0.indexOf('s'));
	      System.out.println( Text0.indexOf('a', 4));
	      System.out.println(Text0.indexOf("que")); 
	      System.out.println( Text0.indexOf("que", 6));
	      */
		//isempty()
		/*
		String par1 = "hola";
	    String par2 = "";
	    System.out.println(par1.isEmpty());
	    System.out.println(par2.isEmpty());
		*/
		//endsWith()
		/*
		System.out.println(Text0.endsWith("e"));
		System.out.println(Text0.endsWith("a"));
		*/
		
		//CompareignoreCase()
		/*
		System.out.println("hola".compareToIgnoreCase("HOLA"));
		System.out.println("hola".compareToIgnoreCase("BOLP"));
		*/
		
		//lastIndexOf()
		/*
		String par3="hola bon dia bon";
		System.out.println(par3.lastIndexOf("o",8));
		System.out.println(par3.lastIndexOf("o"));
		*/
		//matches 
		/*
		String text = "abc123";
		boolean esmatch = text.matches("[a-z]+\\d+");
		System.out.println(esmatch); //  true
		*/
		
		//replace
		String txt = "hola, mon!";
		/*
		
		String txtcanviat = txt.replace("o", "0");
		System.out.println(txtcanviat); // hOla MOn
	   */
		//Replaceall()
		/*
		String txtca = txt.replaceAll("[aeiou]", " ");//canvi dels vocals per un espai
		System.out.println(txtca); 
		*/
		//subtring()
		/*
		String substr1 = txt.substring(6); 
		String substr2 = txt.substring(0, 5); 
		System.out.println(substr1+" "+substr2);
		*/
		//tolowerCase()
		/*
		String str = "HOLA, MON";
		System.out.println(str.toLowerCase());
		 */
		//toUpperCase()
		/*
		System.out.println(txt.toUpperCase());
		*/
		//startsWith()
		/*
		String nom = "Aissam";
		System.out.println(nom.startsWith("A"));
		*/
		//trim()
		/*
		String stri = "   Hola, mon!   ";
		System.out.println(stri.trim()); 
		*/
		char car = 'A';
		System.out.println(Character.valueOf(car)); 


		
		
		lectura.close();		
	}
}
