package abouzama_UF4_Be1_E1;

public class Exercici1 {
	private String Text;
	
	public int comptarVegadesquiapareixlae(){
		int i = 0;
		int comptador=0;
		while(i<Text.length()) {
			if(Text.charAt(i)=='e' || Text.charAt(i)=='E') {
				comptador++;
			}
			i++;
		}
		return comptador;
	}


	public String getText() {
		return Text;
	}


	public void setText(String text) {
		this.Text = text;
	}
	

}
